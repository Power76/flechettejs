const { Game } = require('./Game');

const inquirer = require('inquirer');
const game = new Game();



console.log("Nouvelle partie")

let players = []

const question = {
    type: 'input',
    name: 'nbPlayers',
    message: 'Combien y a t il de joueurs ?',
    default: '2',
    validate: (value) => {
        value = parseInt(value)
        if (typeof value === 'number' && value >= 2){
            // console.log(`\nIl y a ${value} joueurs a cette partie`)
            return true
        } else {
            return 'Entrer un nombre valide'
        }
    }
}

const questions = [
    {
        type: 'list',
        name: 'gamemode',
        message: 'A quel mode de jeu voulez vous jouer ?',
        choices: ['Tour du Monde', '301', 'Cricket']
    }
]



inquirer.prompt(question).then(answers => {
    for (let i = 1; i <= answers.nbPlayers; i++){
        questions.push({
            type: 'input',
            name: `player${i}Name`,
            message: `Comment s'appelle ce joueur : Player${i} ?`
        })
    }
    inquirer.prompt(questions).then(answers => {
        console.log(answers)
        const playersIds = Object.keys(answers).slice(1)
        const playersNames = Object.values(answers).slice(1)

        players.push({
            [Object.keys(answers)[0]]: Object.values(answers)[0]
        })
        for (let i in playersIds){
            players.push({
                name: playersNames[i],
                score: 0
            })
        }


        //console.log(players)
        console.table(players)
        console.log(`Il y a ${playersIds.length} joueurs qui souhaitent faire une partie de ${answers.gamemode}`)

        game.selectGame(players)
    })
})

