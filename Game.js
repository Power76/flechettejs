const { EventEmitter } = require('events')
const { TourDuMonde } = require('./gamemodes/TourDuMonde')
const { game301 } = require('./gamemodes/game301')
const { Cricket } = require('./gamemodes/Cricket')

const tourDuMonde = new TourDuMonde()
const Game301 = new game301()
const cricket = new Cricket()

class Game extends EventEmitter{
    constructor(){
        super();
    }

    selectGame = (players) => {
        switch (players[0].gamemode) {
            case 'Tour du Monde':
                tourDuMonde.startGame(players)
                break
            case '301':
                Game301.startGame(players)
                break
            case 'Cricket':
                cricket.startGame(players)
                break
        }
    }
}

module.exports = {Game}


