const { Game } = require('../Game');
const inquirer = require('inquirer');


class TourDuMonde {
    constructor(){
    }

    startGame = async (players) => {
        // console.log('bravo')

        let continueGame = true
        let finishedPlayers = []
        let currentPlayerNb = 0
        let incr = 1


        players.splice(0, 1)
        players.forEach((i) => {
            i.score = 1
        })
        console.table(players)

        const question = {
            type: 'input',
            name: 'lancer',
            message: "Quel est le resultat du lancer ?",
            default: 0,
            validate: (value) => {
                value = parseInt(value)
                if (typeof value === 'number' && ((value >= 0 && value <= 20) || value === 25)) {
                    return true
                } else {
                    return 'Entrer un nombre valide : le numero de la zone, 0 si hors de la cible, ou 25 au centre'
                }
            }
        }

        while (continueGame) {
            console.table(players)
            console.log(`C'est au tour de ${players[currentPlayerNb % (players.length)].name}`)
            for (let i = 0; i < 3; i++) {
                console.log(`il reste ${3 - i} lancers`)
                console.log(`Zone cible : ${players[currentPlayerNb % (players.length)].score}`)
                await inquirer.prompt(question).then(answers => {
                    if (answers.lancer == players[currentPlayerNb % (players.length)].score) {
                        players[currentPlayerNb % (players.length)].score += 1
                        console.log('réussi')
                        // console.log(`Prochaine zone : ${players[currentPlayerNb % (players.length)].score} `)
                        console.log('')
                        if (players[currentPlayerNb % (players.length)].score === 21) {
                            // continueGame = false
                            console.log(`${players[currentPlayerNb % (players.length)].name} a terminé !`)
                            const finishedPlayer = players.splice(currentPlayerNb % (players.length), 1)
                            currentPlayerNb = (currentPlayerNb % ((players.length)+1))-1
                            finishedPlayers.push(finishedPlayer[0])
                            i = 2


                            if (players.length === 1) {
                                continueGame = false
                                finishedPlayers.push(players[0])
                            }
                        }
                    }
                })
                if (continueGame === false) {
                    console.log("partie terminée")
                    finishedPlayers.forEach((i) => {
                        i.rank = incr
                        incr++
                    })
                    console.table(finishedPlayers)
                    break
                }
            }
            currentPlayerNb++
        }

    }

}

module.exports = {TourDuMonde}
