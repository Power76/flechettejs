const { Game } = require('../Game');
const inquirer = require('inquirer');

class Cricket {
    constructor() {
    }

    startGame = async (players) => {
        console.log('test cricket')

        let continueGame = true
        let outedPlayer = false
        let incr = 1
        let finishedPlayers = []
        let countPoints = true
        let currentPlayerNb = 0
        let tempPointLimit = 0
        let shot = 0
        let multiply = 1
        const listZonesEx = [15, 16, 17, 18, 19, 20, 25]
        let listZones = {
            15: '',
            16: '',
            17: '',
            18: '',
            19: '',
            20: '',
            25: ''
        }


        players.splice(0, 1)
        players.forEach((i) => {
            i.openZonesT = []
            i.openZonesC = []
        })
        console.log(players)



        const questions = [
            {
                type: 'input',
                name: 'lancer',
                message: "Quelle zone a été touchée du lancer ?",
                default: 0,
                validate: (value) => {
                    value = parseInt(value)
                    if (typeof value === 'number' && ((value >= 0 && value <= 20) || value === 25)) {
                        return true
                    } else {
                        return 'Entrer un nombre valide : le numero de la zone, 0 si hors de la cible, ou 25 au centre'
                    }
                }
            },
            {
                type: 'list',
                name: 'zone',
                message: 'Dans quelle zone a atteri la fléchette ?',
                choices: ['Simple', 'Double', 'Triple']
            }
        ]

        while (continueGame) {
            console.table(players)
            console.log(`C'est au tour de ${players[currentPlayerNb % (players.length)].name}`)

            for (let i = 0; i < 3; i++) {
                console.log(`il reste ${3 - i} lancers`)

                await inquirer.prompt(questions).then(answers => {
                    shot = Object.values(answers)[0]
                    switch (Object.values(answers)[1]) {
                        case 'Simple' :
                            multiply = 1
                            break
                        case 'Double' :
                            multiply = 2
                            break
                        case 'Triple' :
                            multiply = 3
                            break
                    }
                    // si la flechette touche une zone valide
                    if (Object.keys(listZones).includes(shot)){
                        // si la flechette touche une zone que le joueur a fermé
                        if (players[currentPlayerNb % (players.length)].openZonesT.includes(shot)){

                            //est ce que le joueur peut marquer des points ?
                            if (players[currentPlayerNb % (players.length)].name === listZones[shot]){
                                players[currentPlayerNb % (players.length)].score += shot
                            }

                        } else {
                            //si la zone n'est pas encore fermée


                            //si la zone a commencé à être fermée
                            if (players[currentPlayerNb % (players.length)].openZonesC.includes(shot)){
                                const shooted = players[currentPlayerNb % (players.length)].openZonesC.find(i => Object.keys(i) === shot)
                                const shootedIdInArray = players[currentPlayerNb % (players.length)].openZonesC.indexOf(shooted)
                                players[currentPlayerNb % (players.length)].openZonesC[shootedIdInArray][shot] += multiply
                            } else {
                                // si la zone n'a pas commencé à être fermée
                                players[currentPlayerNb % (players.length)].openZonesC.push(
                                    {
                                        [shot]: multiply
                                    }
                                )
                            }

                            // setTimeout(() => {console.log(players[currentPlayerNb % (players.length)].openZonesC)}, 1000)
                            const shooted = players[currentPlayerNb % (players.length)].openZonesC.find(i => Object.keys(i) === shot)
                            const shootedIdInArray = players[currentPlayerNb % (players.length)].openZonesC.indexOf(shooted)
                            // si le tir ferme la zone pour le joueur


                            // code trop rapide, shot illisible, pas encore enregistré
                            if (players[currentPlayerNb % (players.length)].openZonesC[shootedIdInArray][shot] >= 3){
                                players[currentPlayerNb % (players.length)].openZonesC.splice(shootedIdInArray, 1)
                                players[currentPlayerNb % (players.length)].openZonesT.push(shot)
                                players[currentPlayerNb % (players.length)].openZonesT.sort()

                                //si le joueur est le premier à fermer cette zone
                                if (listZones[shot] === ''){
                                    listZones[shot] = players[currentPlayerNb % (players.length)].name
                                }

                                // si le joueur est le dernier à fermer la zone
                                if (players.every((i) => {i.openZonesT.includes(shot)})){
                                    // 0 car name forcément string
                                    listZones[shot] = 0
                                }
                            }
                        }
                    }
                    // vérification si le joueur a gagné
                    if (players[currentPlayerNb % (players.length)].openZonesT.equals(listZonesEx) &&
                        players.every((i) => {players[currentPlayerNb % (players.length)].score >= i.score})){

                        console.log(`${players[currentPlayerNb % (players.length)].name} a terminé !`)
                        currentPlayerNb = (currentPlayerNb % ((players.length)+1))-1
                        const finishedPlayer = players.splice(currentPlayerNb % (players.length), 1)
                        finishedPlayers.push(finishedPlayer[0])
                        i = 2


                        if (players.length === 1) {
                            continueGame = false
                            finishedPlayers.push(players[0])
                        }

                    }
                })
                if (continueGame === false) {
                    console.log("partie terminée")
                    finishedPlayers.forEach((i) => {
                        i.rank = incr
                        incr++
                    })
                    console.table(finishedPlayers)
                    break
                }
            }
            currentPlayerNb++
        }
    }
    
}

module.exports = {Cricket}
