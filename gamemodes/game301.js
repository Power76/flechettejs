const { Game } = require('../Game');
const inquirer = require('inquirer');

class game301 {
    constructor(){
    }

    startGame = async (players) => {
        console.log('test 301')

        let continueGame = true
        let outedPlayer = false
        let incr = 1
        let finishedPlayers = []
        let countPoints = true
        let currentPlayerNb = 0
        let tempPointLimit = 0
        let shot = 0
        let multiply = 1


        players.splice(0, 1)
        players.forEach((i) => {
            i.score = 301
        })
        console.log(players)


        const questions = [
            {
                type: 'input',
                name: 'lancer',
                message: "Quelle zone a été touchée du lancer ?",
                default: 0,
                validate: (value) => {
                    value = parseInt(value)
                    if (typeof value === 'number' && ((value >= 0 && value <= 20) || value === 25)) {
                        return true
                    } else {
                        return 'Entrer un nombre valide : le numero de la zone, 0 si hors de la cible, ou 25 au centre'
                    }
                }
            },
            {
                type: 'list',
                name: 'zone',
                message: 'Dans quelle zone a atteri la fléchette ?',
                choices: ['Simple', 'Double', 'Triple']
            }
        ]

        while (continueGame) {
            console.table(players)
            console.log(`C'est au tour de ${players[currentPlayerNb % (players.length)].name}`)
            tempPointLimit = players[currentPlayerNb % (players.length)].score
            countPoints = true

            for (let i = 0; i < 3; i++) {
                console.log(`il reste ${3 - i} lancers`)
                await inquirer.prompt(questions).then(answers => {
                    // console.log(answers)
                    shot = Object.values(answers)[0]
                    // console.log(shot+ ' test')

                    switch (Object.values(answers)[1]) {
                        case 'Simple' :
                            multiply = 1
                            break
                        case 'Double' :
                            multiply = 2
                            break
                        case 'Triple' :
                            multiply = 3
                            break
                    }

                    // console.log(multiply)
                    // console.log(shot)
                    console.log(`Vous avez marqué ${shot*multiply} points`)
                    tempPointLimit -= (shot*multiply)
                    if (tempPointLimit < 0) {
                        console.log('Vous avez dépassé la limite. Votre tour est terminé et vous ne gagnez aucun point')
                        countPoints = false
                        i = 2
                    } else if (tempPointLimit === 0) {
                        i = 2
                        if (multiply === 2) {
                            console.log('Vous avez fini par un double. Vous avez gagné !')
                            console.log(`${players[currentPlayerNb % (players.length)].name} a terminé !`)
                            currentPlayerNb = (currentPlayerNb % ((players.length)+1))-1
                            const finishedPlayer = players.splice(currentPlayerNb % (players.length), 1)
                            finishedPlayers.push(finishedPlayer[0])
                            outedPlayer = true
                            if (players.length === 1) {
                                continueGame = false
                                finishedPlayers.push(players[0])
                            }
                        } else {
                            console.log("Vous n'avez pas terminé par un double. Votre tour est terminé et vous ne gagnez aucun point")
                            countPoints = false
                        }
                    }
                })
                if (continueGame === false) {
                    console.log("partie terminée")
                    finishedPlayers.forEach((i) => {
                        i.rank = incr
                        incr++
                    })
                    console.table(finishedPlayers)
                    break
                }
                if (i === 2 && countPoints === true && outedPlayer === false){
                    players[currentPlayerNb % (players.length)].score = tempPointLimit
                }
                console.log(`Il reste ${tempPointLimit} points`)
            }
            currentPlayerNb++
            outedPlayer = false
            console.log(currentPlayerNb)
        }
    }
}

module.exports = {game301}
